# Todo

[![vert.x 4.0.0 SNAPSHOT purple](https://img.shields.io/badge/Vert.x%20-4.0.0--SNAPSHOT-blueviolet)](https://vertx.io)

## Building

To launch your tests:

``` bash
./mvnw clean test
```

To package your application:

``` bash
./mvnw clean package
```

To run your application:

``` bash
./mvnw clean compile exec:java
```

## Help

- [Vert.x Documentation](https://vertx.io/docs/)
- [Vert.x Stack Overflow](https://stackoverflow.com/questions/tagged/vert.x?sort=newest&pageSize=15)
- [Vert.x User Group](https://groups.google.com/forum/?fromgroups#!forum/vertx)
- [Vert.x Gitter](https://gitter.im/eclipse-vertx/vertx-users)
